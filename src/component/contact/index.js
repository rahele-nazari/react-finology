import React, { Component } from 'react';
import { Row ,Col,Form,Button } from 'react-bootstrap';
import Map from '../map/index'
import './style.css'

class Contact extends Component {
    render() {
        return (
            <section className='contact'>
                <Row>
                    <h2 className='title center'>Contact Us</h2>
                </Row>
                <Row>
                    <Col sm={6}>
                        <Form className='form'>
                            <Form.Group controlId="formBasicEmail">
                                <Form.Label>Name</Form.Label>
                                <Form.Control type="text" placeholder="Enter your name" />
                            </Form.Group>
                            <Form.Group controlId="formBasicPassword">
                                <Form.Label>Email</Form.Label>
                                <Form.Control type="email" placeholder="Enter your email" />
                            </Form.Group>
                            <Form.Group controlId="exampleForm.ControlSelect1">
                                <Form.Label>Subject</Form.Label>
                                <Form.Control as="select">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </Form.Control>
                            </Form.Group>
                            <Form.Group controlId="exampleForm.ControlTextarea1">
                                <Form.Label>Message</Form.Label>
                                <Form.Control as="textarea" rows="3" />
                            </Form.Group>
                            <Button variant="primary" type="submit" className='send-btn'>
                                Send
                            </Button>
                        </Form>
                    </Col>
                    <Col sm={6}  className='map'>
                        <Map/>
                    </Col>
                </Row>
            </section>
        )
    }
}

export default Contact;

