import React, { Component } from 'react';
import {Image, Row, Col, Button, ButtonToolbar, Modal, Form} from 'react-bootstrap';
import icon from "../../images/7-icons/happy-face.svg";
import tania from '../../images/4-people/tania-ferreira.jpg';
import darlene from '../../images/4-people/darlene-chabrat.jpg';
import pencil from '../../images/7-icons/pencil.svg';
import garbage from '../../images/7-icons/garbage.svg';
import './style.css';
import user from "../../images/7-icons/user.svg";


class People extends Component {
    constructor(...args) {
        super(...args);

        this.state = {
            file: null,
            image: '',
            name: '',
            position: '',
            show: false,
            showEdit: false,
            modalShow: false,
            editState: false,
            editBtn: false,
            edit: [{}],
            index: 0,
            items:[
                {
                   image: tania,
                   name: 'Tania Ferreira',
                   position: 'CEO',
                   index:0
                },
                {
                    image: darlene,
                    name: 'Tania Ferreira',
                    position: 'CEO',
                    index:1
                },
                {
                    image: tania,
                    name: 'Tania Ferreira',
                    position: 'CEO',
                    index:2
                },
                {
                    image: tania,
                    name: 'Tania Ferreira',
                    position: 'CEO',
                    index:3
                }
            ]
        };
        this.handleShow = () => {
            this.setState({ show: true });
        };

        this.handleHide = () => {
            this.setState({ show: false });
        };
        this.handleShowEdit = () => {
            this.setState({ showEdit: true });
        };

        this.handleHideEdit = () => {
            this.setState({ showEdit: false });
        };
        this.handleChange = this.handleChange.bind(this);
        this.listChange = this.listChange.bind(this);
        this.removeList = this.removeList.bind(this);
        this.handleImageChange = this.handleImageChange.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handlePositionChange = this.handlePositionChange.bind(this);
        this.imageFile = this.imageFile.bind(this);
        this.listEdit = this.listEdit.bind(this);
        this.saveChange = this.saveChange.bind(this);
        this.checkBox = this.checkBox.bind(this);
    }

    imageFile(evt) {
        this.setState({
            file: URL.createObjectURL(evt.target.files[0]),
            image: URL.createObjectURL(evt.target.files[0])
        })
    }
    handleImageChange (evt) {
        this.setState({
            image:  URL.createObjectURL(evt.target.files[0])
        });
    }
    handleNameChange (evt) {
        this.setState({
            name: evt.target.value
        });
        console.log(this.state.name)
    }
    handlePositionChange (evt) {
        this.setState({
            position: evt.target.value
        })
    }
    listChange(ev) {
        ev.preventDefault();
        if (  !this.state.image || !this.state.name || !this.state.position) return;
        this.setState(prevState => {
            const newItem = {
                image: this.state.image,
                name: this.state.name,
                position: this.state.position,
                index: this.state.items.length
            };
            return {
                items: prevState.items.concat([newItem]),
                image: '',
                name: '',
                position:'',
                file: null,
                show: false
            };
        });
    }

    listEdit(index){
        this.setState(prevState => {
            return {
                edit: prevState.items.filter((item,i) => i===index),
                index: index,
                showEdit:true,
                file: null
            };
        });
    }

    saveChange(){
        this.setState(prevState => {
            const newItems = prevState.items.slice();
            newItems[prevState.index] = {
                image : prevState.image,
                name : prevState.name,
                position : prevState.position
            };
            return {
                items: newItems,
                showEdit:false
            };
        });
    }
    checkBox(index){
        this.setState(this.setState(prevstate => ({
            editState: !prevstate.eagerState
        })))
    }
    handleChange(index){
        this.setState(prevState => {
            return {
                items: prevState.items.map((item, i) => {
                    return {
                        ...item,
                        checked: i === index ? !item.checked : item.checked
                    };
                })
            };
        });
        console.log(index)
    }
    removeList(){
        this.setState(prevState => {
            return {
                items: prevState.items.filter((item) => !item.checked),
                editState: false
            };
        });
    }
    render(){
        return (
            <section className='people'>
                <Row>
                    <Col sm={1}>
                        <div className='carousel-icon'>
                            <Image src={icon}/>
                        </div>
                    </Col>
                    <Col sm={7} className='text-left'>
                        <h2 className="title">What other people say about our service</h2>
                    </Col>
                    <Col sm={4} className='btn-section'>
                            {
                                this.state.editState ?
                                    <Row>
                                        <Col sm={5} className="cancel" onClick={() => this.setState(this.setState(prevstate => ({ editState: !prevstate.editState,editBtn: !prevstate.editBtn})))}>
                                            <p>Cancel</p>
                                        </Col>
                                        <Col sm={6} className='delete'>
                                            <div className="cont" onClick={() => this.removeList()}>
                                                <img src={garbage} className="img-fluid"/>
                                            </div>
                                        </Col>
                                    </Row>
                                    :
                                    <ButtonToolbar>
                                    <Row>
                                        <Col sm={4}>
                                            <Button
                                                className='edit'
                                                onClick={() => this.setState(prevstate => ({ editBtn: !prevstate.editBtn}))}
                                            >
                                                Edit
                                            </Button>
                                        </Col>
                                        <Col sm={6}>
                                            <Button
                                                className='add'
                                                onClick={this.handleShow}
                                            >
                                                Add
                                            </Button>
                                        </Col>
                                    </Row>

                                        <Modal className='content'
                                               show={this.state.show}
                                               onHide={this.handleHide}
                                               dialogClassName="modal-90w"
                                               aria-labelledby="example-custom-modal-styling-title"
                                        >
                                            <Modal.Header closeButton>
                                                <Modal.Title id="contained-modal-title-vcenter">
                                                    Add people
                                                </Modal.Title>
                                            </Modal.Header>
                                            <Modal.Body>
                                                <div className="user">
                                                    <img src={this.state.file ? this.state.file: user} className="img-fluid" alt="content-002"/>
                                                    <div className="edit">
                                                        <input type="file" id="file" className="inputfile inputfile-5"
                                                               onChange={this.imageFile}/>
                                                        <label htmlFor="file"><img src={pencil} alt="edit"/></label>
                                                    </div>
                                                </div>
                                                <Form className='modal-form'>
                                                    <Form.Group controlId="formBasicName">
                                                        <Form.Label>Name</Form.Label>
                                                        <Form.Control type="text" placeholder="Enter name" onChange={this.handleNameChange} />
                                                    </Form.Group>
                                                    <Form.Group controlId="formBasicPosition">
                                                        <Form.Label>Position</Form.Label>
                                                        <Form.Control type="text" placeholder="Enter Position" onChange={this.handlePositionChange}/>
                                                    </Form.Group>
                                                </Form>
                                            </Modal.Body>
                                            <Modal.Footer>
                                                <Button variant="secondary"  onClick={this.handleHide}>Cancel</Button>
                                                <Button variant="primary" onClick={this.listChange}>Save</Button>
                                            </Modal.Footer>
                                        </Modal>
                                    </ButtonToolbar>
                            }

                    </Col>
                </Row>
                <Row className='people-card'>
                    {
                        this.state.items.map((item,index) => {
                            return(
                                <div className="card" key={index}>
                                    <img src={item.image} className="card-img-top" alt={item.name}/>
                                    {
                                        this.state.editBtn ?
                                            <Row>
                                                <div className="edit"  onClick={() => this.listEdit(index)}>
                                                    <img src={pencil} alt="edit"/>
                                                </div>
                                                <div className="remove">
                                                    <label className="cnt">
                                                        <input type="checkbox"
                                                               checked={item.checked}
                                                               onClick={() => this.checkBox(index)}
                                                               onChange={() => this.handleChange(index)}
                                                        />
                                                        <span className="checkmark"></span>
                                                    </label>
                                                </div>
                                            </Row>
                                            :
                                            null
                                    }
                                    <div className="card-body">
                                        <p className="card-text name">{item.name}</p>
                                        <p className="card-text card-subtitle">{item.position}</p>
                                    </div>
                                </div>
                            )
                        })
                    }
                    <Modal className='content'
                           show={this.state.showEdit}
                           onHide={this.handleHideEdit}
                           dialogClassName="modal-90w"
                           aria-labelledby="example-custom-modal-styling-title"
                    >
                        <Modal.Header closeButton onClick={this.handleHideEdit}>
                            <Modal.Title id="contained-modal-title-vcenter">
                                Edit people
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <div className="user">
                                <img src={this.state.file ? this.state.file : this.state.edit[0].image }
                                     className="img-fluid" alt="content-002"/>
                                <div className="edit">
                                    <input type="file" id="file" className="inputfile inputfile-5"
                                           onChange={this.imageFile}/>
                                    <label htmlFor="file"><img src={pencil} alt="edit"/></label>
                                </div>
                            </div>
                            <Form className='modal-form'>
                                <Form.Group controlId="formBasicName">
                                    <Form.Label>Name</Form.Label>
                                    <Form.Control type="text" placeholder={this.state.edit[0].name}  onChange={this.handleNameChange}/>
                                </Form.Group>
                                <Form.Group controlId="formBasicPosition">
                                    <Form.Label>Position</Form.Label>
                                    <Form.Control type="text" placeholder={this.state.edit[0].position}  onChange={this.handlePositionChange}/>
                                </Form.Group>
                            </Form>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="secondary"  onClick={this.handleHideEdit}>Cancel</Button>
                            <Button variant="primary" onClick={this.saveChange}>Save</Button>
                        </Modal.Footer>
                    </Modal>
                </Row>
            </section>
        )
    }
}

export default People;
