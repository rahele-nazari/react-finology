import React, { Component } from 'react';
import { Container,Image,Row,Col } from 'react-bootstrap';
import Menu from '../menu/index';
import banner from '../../images/2-banner/banner.png';
import './style.css';



class Banner extends Component {
    render() {
        return (
            <Container>
                <Menu/>
                <Row className='banner-desc'>
                    <Col lg={6}>
                        <h1>Make development </h1>
                        <h1>easy with us.</h1>
                        <p>doing development can never be easy,and it takes time and resources.</p>
                        <p>We at EasyWork has the solution.</p>
                    </Col>
                    <Col lg={6}>
                        <Image src={banner} fluid/>
                    </Col>
                </Row>
            </Container>
        )
    }
}

export default Banner;
