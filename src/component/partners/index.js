import React, { Component } from 'react';
import { Row ,Col,Image} from 'react-bootstrap';
import apple from '../../images/5-partners/apple.PNG';
import airbnb from '../../images/5-partners/airbnb.png';
import facebook from '../../images/5-partners/facebook.PNG';
import google from '../../images/5-partners/google.PNG';
import microsoft from '../../images/5-partners/microsoft.PNG';
import nvidia from '../../images/5-partners/nvidia.PNG';
import samsung from '../../images/5-partners/samsung.PNG';
import tesla from '../../images/5-partners/tesla.PNG';


class Partners extends Component {
    render() {
        return (
            <section>
                <Row>
                    <h2 className="title center">We've worked with</h2>
                </Row>
                <Row>
                    <Col><Image src={apple}/></Col>
                    <Col><Image src={airbnb}/></Col>
                    <Col><Image src={facebook}/></Col>
                    <Col><Image src={google}/></Col>
                    <Col><Image src={microsoft}/></Col>
                    <Col><Image src={nvidia}/></Col>
                    <Col><Image src={samsung}/></Col>
                    <Col><Image src={tesla}/></Col>
                </Row>
            </section>
        )
    }
}

export default Partners;
