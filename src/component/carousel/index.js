import React from 'react';
import { Carousel,Row,Col,Image,Container} from 'react-bootstrap';
import people from '../../images/4-people/gaetan-houssin.jpg'
import icon from '../../images/7-icons/heart.svg'
import './style.css'

class ControlledCarousel extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.handleSelect = this.handleSelect.bind(this);

        this.state = {
            index: 0,
            direction: null,
        };
    }

    handleSelect(selectedIndex, e) {
        this.setState({
            index: selectedIndex,
            direction: e.direction,
        });
    }

    render() {
        const { index, direction } = this.state;

        return (
            <section>
                <Container>
                    <Row>
                        <Col sm={1}>
                            <div className='carousel-icon'>
                                <Image src={icon}/>
                            </div>
                        </Col>
                        <Col sm={7} className='text-left'>
                            <h2 className="title">What other people say about our service</h2>
                        </Col>
                    </Row>
                </Container>
                <Carousel
                    activeIndex={index}
                    direction={direction}
                    onSelect={this.handleSelect}
                >
                    <Carousel.Item>
                        <Row>
                            <Col lg={4}>
                                <div className="comment">
                                    <div className="flex-row top">
                                        <div className="people">
                                            <Image src={people}
                                                   alt="content-002"/>
                                        </div>
                                        <div>
                                            <p className="name">David Champion</p>
                                            <p>CEO of iCloud</p>
                                        </div>
                                    </div>
                                    <div>
                                        <p>"System is excellent. It has made my system user experience to become one of the
                                            easiest!"</p>
                                    </div>
                                </div>
                            </Col>
                            <Col lg={4}>
                                <div className="comment">
                                    <div className="flex-row top">
                                        <div className="people">
                                            <Image src={people}
                                                   alt="content-002"/>
                                        </div>
                                        <div>
                                            <p className="name">David Champion</p>
                                            <p>CEO of iCloud</p>
                                        </div>
                                    </div>
                                    <div>
                                        <p>"System is excellent. It has made my system user experience to become one of the
                                            easiest!"</p>
                                    </div>
                                </div>
                            </Col>
                            <Col lg={4}>
                                <div className="comment">
                                    <div className="flex-row top">
                                        <div className="people">
                                            <Image src={people}
                                                   alt="content-002"/>
                                        </div>
                                        <div>
                                            <p className="name">David Champion</p>
                                            <p>CEO of iCloud</p>
                                        </div>
                                    </div>
                                    <div>
                                        <p>"System is excellent. It has made my system user experience to become one of the
                                            easiest!"</p>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </Carousel.Item>
                    <Carousel.Item>
                        <Row>
                            <Col lg={4}>
                                <div className="comment">
                                    <div className="flex-row top">
                                        <div className="people">
                                            <Image src={people}
                                                   alt="content-002"/>
                                        </div>
                                        <div>
                                            <p className="name">David Champion</p>
                                            <p>CEO of iCloud</p>
                                        </div>
                                    </div>
                                    <div>
                                        <p>"System is excellent. It has made my system user experience to become one of the
                                            easiest!"</p>
                                    </div>
                                </div>
                            </Col>
                            <Col lg={4}>
                                <div className="comment">
                                    <div className="flex-row top">
                                        <div className="people">
                                            <Image src={people}
                                                   alt="content-002"/>
                                        </div>
                                        <div>
                                            <p className="name">David Champion</p>
                                            <p>CEO of iCloud</p>
                                        </div>
                                    </div>
                                    <div>
                                        <p>"System is excellent. It has made my system user experience to become one of the
                                            easiest!"</p>
                                    </div>
                                </div>
                            </Col>
                            <Col lg={4}>
                                <div className="comment">
                                    <div className="flex-row top">
                                        <div className="people">
                                            <Image src={people}
                                                   alt="content-002"/>
                                        </div>
                                        <div>
                                            <p className="name">David Champion</p>
                                            <p>CEO of iCloud</p>
                                        </div>
                                    </div>
                                    <div>
                                        <p>"System is excellent. It has made my system user experience to become one of the
                                            easiest!"</p>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </Carousel.Item>
                    <Carousel.Item>
                        <Row>
                            <Col lg={4}>
                                <div className="comment">
                                    <div className="flex-row top">
                                        <div className="people">
                                            <Image src={people}
                                                   alt="content-002"/>
                                        </div>
                                        <div>
                                            <p className="name">David Champion</p>
                                            <p>CEO of iCloud</p>
                                        </div>
                                    </div>
                                    <div>
                                        <p>"System is excellent. It has made my system user experience to become one of the
                                            easiest!"</p>
                                    </div>
                                </div>
                            </Col>
                            <Col lg={4}>
                                <div className="comment">
                                    <div className="flex-row top">
                                        <div className="people">
                                            <Image src={people}
                                                   alt="content-002"/>
                                        </div>
                                        <div>
                                            <p className="name">David Champion</p>
                                            <p>CEO of iCloud</p>
                                        </div>
                                    </div>
                                    <div>
                                        <p>"System is excellent. It has made my system user experience to become one of the
                                            easiest!"</p>
                                    </div>
                                </div>
                            </Col>
                            <Col lg={4}>
                                <div className="comment">
                                    <div className="flex-row top">
                                        <div className="people">
                                            <Image src={people}
                                                   alt="content-002"/>
                                        </div>
                                        <div>
                                            <p className="name">David Champion</p>
                                            <p>CEO of iCloud</p>
                                        </div>
                                    </div>
                                    <div>
                                        <p>"System is excellent. It has made my system user experience to become one of the
                                            easiest!"</p>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </Carousel.Item>
                </Carousel>
            </section>
        );
    }
}


export default ControlledCarousel;