import React, { Component } from 'react';
import { Navbar,Nav,Form,FormControl } from 'react-bootstrap';
import logo from '../../images/1-header/logo.png';
import search from '../../images/7-icons/search.svg';


class Menu extends Component {
    render() {
        return (
            <Navbar expand="lg">
                <Navbar.Brand href="#home">
                    <img src={logo} alt='finology'/>
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        <Nav.Link href="#home">Home</Nav.Link>
                        <Nav.Link href="#link">About Us</Nav.Link>
                        <Nav.Link href="#link">Services</Nav.Link>
                        <Nav.Link href="#link">Pricing</Nav.Link>
                        <Nav.Link href="#link">Careers</Nav.Link>
                    </Nav>
                    <Form inline>
                        <FormControl type="text" className="mr-sm-2" />
                        <img src={search} alt='search'/>
                    </Form>
                </Navbar.Collapse>
            </Navbar>
        )
    }
}

export default Menu;
