import React, { Component } from 'react';
import { Row,Col,Image,Container } from 'react-bootstrap';
import logo from '../../images/1-header/logo.png';
import './style.css'


class Footer extends Component {
    render() {
        return (
            <Container>
                <div className='footrer'>
                    <Row>
                        <Col sm={4}>
                            <Row>
                                <Col sm={4}>
                                    <Image src={logo}/>
                                </Col>
                                <Col sm={8}>
                                    <h3>Easy Work</h3>
                                </Col>
                            </Row>
                        </Col>
                        <Col sm={3}>
                            <h4>Address</h4>
                            <p>
                                52-1, Jalan Awan Hijau, Taman Overseas Union, 58200 Kuala Lumpur, Wilayah Persekutuan Kuala Lampur
                            </p>
                        </Col>
                        <Col sm={5}>
                            <h4>Contact</h4>
                            <p>03-7451 5283</p>
                            <h4>Fax</h4>
                            <p>03-7451 5283</p>
                        </Col>
                    </Row>
                </div>
            </Container>
        )
    }
}

export default Footer;
