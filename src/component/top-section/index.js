import React, { Component } from 'react';
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';
import { Image } from 'react-bootstrap';
import contenct1 from '../../images/3-content/content-001.png';
import contenct2 from '../../images/3-content/content-002.png';


class TopSection extends Component {
    render() {
        return (
            <section>
                <Row>
                    <Col lg={7}>
                        <Image src={contenct1}  fluid/>
                    </Col>
                    <Col lg={5}>
                        <h2 className="title">We can give you our best exprience to your system</h2>
                        <p>
                            Lorem Impusm is simply dummy text of the printing and typesetting industry. Lorem Impsum has
                            been the industry's standard dummy text ever since the 1500s. When an unknown printer took a
                            galley of type and scrambled it to make a type specimen book. It has survived not only five
                            centuries, but also the leap into electronic typesetting, remaining essentially
                            unchanged
                        </p>
                    </Col>
                </Row>
                <Row>
                    <Col lg={5}>
                        <h2 className="title">Easy access, Whenever, wherever you want</h2>
                        <p>
                            Lorem Impusm is simply dummy text of the printing and typesetting industry. Lorem Impsum has
                            been the industry's standard dummy text ever since the 1500s. When an unknown printer took a
                            galley of type and scrambled it to make a type specimen book. It has survived not only five
                            centuries, but also the leap into electronic typesetting, remaining essentially
                            unchanged
                        </p>
                    </Col>
                    <Col lg={7}>
                        <Image src={contenct2}  fluid/>
                    </Col>
                </Row>
            </section>
        )
    }
}

export default TopSection;
