import React, { Component } from 'react';
import './App.css';
import Banner from './component/banner/index'
import TopSection from "./component/top-section";
import Partners from "./component/partners/index";
import ControlledCarousel from "./component/carousel/index";
import { Container } from 'react-bootstrap';
import Contact from "./component/contact";
import Footer from "./component/footer";
import People from "./component/people";

class App extends Component {
  render() {
    return (
      <div className="App">
        <section className='banner'>
          <Banner/>
        </section>
        <Container className='top-section'>
          <TopSection/>
        </Container>
        <ControlledCarousel/>
        <Container>
          <People/>
          <Partners/>
          <Contact/>
        </Container>
        <section className='footer'>
           <Footer/>
        </section>
      </div>
    );
  }
}

export default App;
